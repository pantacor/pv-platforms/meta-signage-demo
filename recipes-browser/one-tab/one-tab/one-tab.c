/*
 * Copyright (C) 2019 Pantacor Ltd
 * Copyright (C) 2006, 2007 Apple Inc.
 * Copyright (C) 2007 Alp Toker <alp@atoker.com>
 * Copyright (C) 2011 Lukasz Slachciak
 * Copyright (C) 2011 Bob Murphy
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE COMPUTER, INC. ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE COMPUTER, INC. OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtk/gtk.h>
#include <webkit2/webkit2.h>


static void destroyWindowCb(GtkWidget* widget, GtkWidget* window);
static struct LocalState* _state_init(int *argc, char **argv[]);

struct LocalState {
	WebKitWebView *webView;

	int i;
	int urlc;
	char **urlv;
	int *timeoutv;
	gboolean donotrepeat;
};

static gboolean on_key_press (GtkWidget *widget, GdkEventKey *event, gpointer data) {
    struct LocalState *s = (struct LocalState*) data;

    if (event->keyval == GDK_KEY_Left &&
              event->state & GDK_CONTROL_MASK &&
              webkit_web_view_can_go_back(s->webView)) {
        webkit_web_view_go_back(s->webView);
        return TRUE;
    }
    if (event->keyval == GDK_KEY_Right &&
              event->state & GDK_CONTROL_MASK &&
              webkit_web_view_can_go_forward(s->webView)) {
        webkit_web_view_go_forward(s->webView);
        return TRUE;
    }
    return FALSE;
}

static gboolean update_page(gpointer data) {

    struct LocalState *s = (struct LocalState*) data;

    webkit_web_view_load_uri(s->webView, s->urlv[s->i]);

    guint tid = g_timeout_add_seconds (s->timeoutv[s->i], update_page, s);
    s->i = (s -> i + 1) % s->urlc;

    // if we would start over but have donotrepeat set to true we cancel the timeout just added.
    if (s->i == 0 && s->donotrepeat)
	    g_source_remove(tid);

    return FALSE;
}

static struct LocalState* _state_init(int *argc, char **argv[]) {
    struct LocalState* state = g_malloc(sizeof(struct LocalState));
    const gchar *env_donotrepeat;
    const gchar *env_timeout;
    gint timeout = 10;

    state->i = 0;

    if (*argc <=1) {
	    state->urlc = 1;
	    state->urlv = calloc(sizeof(char*), 1);
	    state->urlv[0] = g_strdup("http://www.google.com");
	    state->timeoutv = calloc(sizeof(int), 1);
	    state->timeoutv[0] = 10;
	    g_print("url %s \n", state->urlv[0]);
	    return state;
    }

    state->urlc = *argc - 1;
    state->urlv = calloc (sizeof(char*), *argc - 1);
    state->timeoutv = calloc(sizeof(int), *argc - 1);
    for (int i=0; i< *argc - 1; i++)
        state->urlv[i] = g_strdup((*argv)[i+1]);

    env_donotrepeat = g_getenv("ONE_TAB_DONOTREPEAT");
    if (env_donotrepeat && strlen(env_donotrepeat))
        state->donotrepeat = TRUE;

    env_timeout = g_getenv("ONE_TAB_TIMEOUT");
    if (env_timeout && strlen(env_timeout)) {
        gint t = strtol(env_timeout, NULL, 10); 
        if (t >= 0)
	    timeout = t;
    }

    for (int i=0; i< *argc - 1; i++)
        state->timeoutv[i] = timeout;

    return state;
}

int main(int argc, char* argv[])
{
    struct LocalState *state;
    // Initialize GTK+
    gtk_init(&argc, &argv);
    state = _state_init (&argc, &argv);

    // Create an 800x600 window that will contain the browser instance
    GtkWidget *main_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(main_window), 800, 600);
    gtk_window_fullscreen(GTK_WINDOW(main_window));

    // Create a browser instance
    WebKitWebView *webView = WEBKIT_WEB_VIEW(webkit_web_view_new());

    // Put the browser area into the main window
    gtk_container_add(GTK_CONTAINER(main_window), GTK_WIDGET(webView));

    // Set up callbacks so that if either the main window or the browser instance is
    // closed, the program will exit
    g_signal_connect(main_window, "destroy", G_CALLBACK(destroyWindowCb), NULL);
    g_signal_connect (G_OBJECT (main_window), "key_press_event", G_CALLBACK (on_key_press), state);


    state->webView = webView;

    update_page(state);

    // Make sure that when the browser area becomes visible, it will get mouse
    // and keyboard events
    gtk_widget_grab_focus(GTK_WIDGET(webView));

    // Make sure the main window and all its contents are visible
    gtk_widget_show_all(main_window);

    // Run the main GTK+ event loop
    gtk_main();

    return TRUE;
}


static void destroyWindowCb(GtkWidget* widget, GtkWidget* window)
{
    gtk_main_quit();
}

