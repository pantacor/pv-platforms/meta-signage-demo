LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

inherit cmake pkgconfig
DEPENDS = "gtk+3 webkitgtk pango cairo gdk-pixbuf atk webkitgtk libsoup-2.4"
RDEPEND_${PN} = "gtk+3 webkitgtk pango cairo gdk-pixbuf atk webkitgtk libsoup-2.4"

S = "${WORKDIR}"
SRC_URI = "file://one-tab.c \
	   file://CMakeLists.txt \
	"

