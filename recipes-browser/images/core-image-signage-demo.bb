DESCRIPTION = "A basic image with one tab full screen browser integrated"

LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=b97a012949927931feb7793eee5ed924 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

IMAGE_FEATURES += "splash package-management ssh-server-openssh x11-base"

PR="r1"

LICENSE = "MIT"

inherit core-image

IMAGE_INSTALL += " packagegroup-x11-browser one-tab one-tab-session"
