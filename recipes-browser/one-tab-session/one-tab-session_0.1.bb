DESCRIPTION = "One Tab X session files for poky"
HOMEPAGE = "http://www.yoctoproject.org"
BUGTRACKER = "http://bugzilla.pokylinux.org"

PR = "r4"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://one-tab-session;endline=5;md5=caa15bc7675a9d01becce7b6d24a8861 \
                    "

SECTION = "x11"
RCONFLICTS_${PN} = "matchbox-common"

SRC_URI = "file://one-tab-session \
           file://one-tab-session.conf \
          "
S = "${WORKDIR}"

inherit update-alternatives

BROWSER="midori"
USER="root"

ALTERNATIVE_${PN} = "x-session-manager"
ALTERNATIVE_TARGET[x-session-manager] = "${bindir}/one-tab-session"
ALTERNATIVE_PRIORITY = "90"

do_install() {
        install -d ${D}/${bindir}
        install -d ${D}/${sysconfdir}
        install -m 0755 ${S}/one-tab-session ${D}/${bindir}
        install -m 0755 ${S}/one-tab-session.conf ${D}/${sysconfdir}
}

